                       
	CSS Guidelines Version 0.1
	
	Diese Vorgaben sollen zur Vereinfachung und Vermeidung von Fehlern und zur Steigerung der �bersicht f�hren.
	Alles ist offen f�r Verbesserungsvorschl�ge. 
	        
	
	FILES:
	- Logisch zusammengeh�rende Klassen werden in ein File zusammengefasst (Components). Der Name des Files 
	  wiederspiegelt den Inhalt: Hauptklasse .preview > preview.css
	  Gr�ssere Konstrukte haben ihr eigenes CSS > preview.css
   
	- Kleiner k�nnen in einem zusammenfassenden File sein (z.B. Fehlermeldungen, verschidene 
	  Formen von Inputs...). �ber die Verwendung von Files soll gesprochen werden.
	
	- IE-Fixes haben ein eigenes Files das dem Namen des Hauptfiles entspricht mit -ie am Ende: preview-ie.css
	  Dies dient dazu, in den Hauptfiles nur Validen Code zu haben  
	
	- Breitendefinitionen f�r colgroups haben ihr eigenes Files: colgroup.css
	
	- Components geh�ren in den components Ordner  
                    


    AUFBAU DER FILES

	1. Grundelement, Hauptklasse von der alle anderen Varianten abgeleitet werden (.grundelement) 
	2. Inneren Elemente des Grundelemens (.preview > h4)
	3. Varioationen f�r die inneren Elemente (.preview > h4.typ2)
	4. Varioationen f�r das Grundelement
	5. Seitenspezifische Variationen 
	
	Beispiel:                                              
		/* general definition */
		.preview				{ margin: 10px; background-color: #f00; }  
		.preview  h4			{ margin: 0; }		

		/* sub-variations */                                                                             
		.preview  h4.type2		{ margin-top: 15px; }		

		/* variations */
		.previewDoc 			{ background-image: url(../../img/img.gif); background-position: top right; background-repeat: no-repeat; } 

		/* site-specific extensions */
		#s3319-00 .preview		{ width: 150px; }
   		#s3025-00 .preview		{ width: 152px; }



    CSS: ALLGEMEIN
    
	- Jede Seite ist von einem Element mit einer eindeutigen, projektweit einzigartigen ID umschlossen die sich aus
	  der TP Storynummer gefolgt von einer vortlaufenden Zahl je weiteres Template der Story ergibt: 
	  z.B. #s3319-00                                           
	
	- Keine Hacks in den normalen Files             
	
    - Keine Importants! (Ausnahmen m�ssen besprochen werden.)   

    - Paddings wo m�glich vermeiden und stattdessen margins verwenden. Grund: das Padding eines �usseren Elements
	  kann vom Kind nicht mehr ver�ndert werden, das innere Element kann aber immer noch durch margin einen Abstand
	  zum �usserden Element schaffen.   
	
	- Wo keine vollst�ndige Deklaration einer Eigenschaft erfolgt, darf nicht die abgek�rtzte CSS Notation verwendet 
	  werden, damit nichts aus einer Vererbung �berschrieben wird. Nur wenn explizit ein Reset durchgef�hrt werden 
	  soll ist die abgek�rzte Version zul�ssig:
	  NICHT 	{ background: #fff; font: bold; }
	  SONDERN  	{ background-color: #fff; font-weight: bold; }
	  ABER		{ background: #fff url(../../img/image.gif) top left no-repeat; }    
	
	- Man sollte sich sehr gut �berlegen, ob man einer Klasse eine Breite mitgibt. Besser ist es wohl, diese im 
	  Seitenspezifischen Bereich zu erg�nzen:
	  .preview			 { margin: 15px; }
	  #s3319-00 .preview { width: 579px; }

             

	CSS: PRIORIT�TEN
	
	- Alles was ins new_base.css kommt muss m�glichst neutral sein und eine sehr niedere Priorit�t haben! 
	  Das Owenship vom new_base.css liegt bei Noel. Alle �nderungen und erg�nzugen m�ssen besprochen werden.  
		
	- Styles direkt auf ein DOM Element ohne Klasse sind praktisch nicht zul�ssig:
	  NICHT		ul 	   
	  SONDER	ul.klasse
	  ODER		.klasse ul
	
	- Definitionen m�ssen mit kleinstm�glicher Priorit�t gehalten werden um das �berschreiben zu erm�glichen:
	  NICHT 	#id .klasse
				.klasse ul li
	  SONDERN	.klasse
				.klasse li

   - IDs sind praktisch zu immer unn�tig und nur in ganz speziellen F�llen zu verwenden. Die Verwendung
	 von IDs f�r Components im ganzen Projekt d�rfte sich auf eine Hand voll reduzieren lassen.
	 Grund: IDs haben eine sehr hohe Priorit�t und lassen sich nur schwer erweitern / �berschreiben.
				
					              
	
	CSS: NAMING         
	
	- Nummern zu begin der Klasse / ID sind verboten: 
	
	- Klassen werden klein geschrieben. Erweiterungsklassen setzen sich aus der Hauptklasse + Grossgeschrieben der
	  Erweiterungsname zusammen:
	  Mitterklasse: .preview
	  Erweiterung: 	.previewImage 
	
	- Bei unklarem oder weitem Verwendungszweck wird die Erweiterung mit einer fortlaufenden Nummer angegeben:
	  .preview2
	
	- Erweiterungen zu einener Mutterklassen enthalten im HTML die Mutterklasse, plus die Erweiterung. Damit
	  erhalten sie die Grunddefinition plus die weiteren, erg�nzten oder �berschrieben Attribute. Es ist nur 
	  eine (1) erweiterung zul�ssig. Wenn das nicht reicht, muss eine neue Erweiterung angelegt werden.
	  <div class="preview previewImage"> ...
	  <div class="linegroup linegroup2"> ...
			

	
	
	� 2008 by namics ag